# Cookiecutter template for a snakemake based data analysis project at LSI

## Usage

```
    pip install cookiecutter
    cookiecutter bb:princeton_genomics/cookiecutter-snakemake-project.git
```

You will be prompted for basic information which is used in the template.

## Project Structure

```
    ./
    |-- project_description.yaml  <- Project description file for https://bitbucket.org/princeton_genomics/project_archive
    |-- README.md

    |-- Snakefile                 <- The main entrypoint for the workflow
    |-- config.yml.sample        <- Config template
    |-- environment.yml          <- Conda environment.yml file for base environment
    |-- rules/                    <- Collection of rules imported in the main Snakefile
    |-- scripts/                  <- Scripts used for this project
    |-- scripts/common/           <- Python package used in multiple scripts

    |-- cluster_config.cetus.yml  <- Cluster config file for cetus
    |-- run_snakemake_cetus.sh    <- Script to simplify execution on cetus

    |-- data/                     <- The original, immutable data
    |-- results                   <- Derived (reproducible) results, not stored in git

    |-- .travis.yml               <- Enable travis testing using data in .test
    |-- .test/                    <- Test cases

    |-- LICENSE
    |-- .editorconfig             <- Options for EditorConfig (http://EditorConfig.org)
    |-- .gitattributes            <- Set linguist-language (for GitHub)
    |-- .gitignore
```
